import serial
import json
import time

ser = serial.Serial("/dev/ttyACM0", 9600)

with open("data.json", "r") as f:
    summary = json.load(f)

print("Waiting for arduino")
while True:
    zeile = ser.readline()
    if zeile.decode().startswith("started"):
        break
    time.sleep(0.1)

# while True:
while True:
    for video in summary:
        print("sending information for " + video["titel"])
        ser.write(f"title:{video['titel']}\n".encode())
        ser.write(f"pos:{int(video['pos'])}\n".encode())
        ser.write(f"neu:{int(video['neu'])}\n".encode())
        ser.write(f"neg:{int(video['neg'])}\n".encode())
        while True:
            zeile = ser.readline()
            if zeile.decode().startswith("next"):
                break
            time.sleep(0.1)
