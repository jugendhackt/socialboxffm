# setup
import pandas as pd
import requests
from bs4 import BeautifulSoup

# read csv data
csv = pd.read_csv("comments_funk_jugend_hack_ffm23.csv")

# setup variables and lists
list_positive = 0
list_neutral = 0
list_negative = 0
ratinglist = csv["sentiment_multiclass"]
videolist = csv["video_id"]
video_list = csv["video_id"].unique()
summery = []

# count positive, neutral and negative ratings
for video_id in video_list:
    # filter all comments with this video_id
    video_comments = csv.loc[csv["video_id"] == video_id]
    list_positive = 0
    list_neutral = 0
    list_negative = 0
    for index, row in video_comments.iterrows():
        if row["sentiment_multiclass"] == "POSITIVE":
            list_positive = list_positive + 1
        if row["sentiment_multiclass"] == "NEUTRAL":
            list_neutral = list_neutral + 1
        if row["sentiment_multiclass"] == "NEGATIVE":
            list_negative = list_negative + 1
    print(
        f"Positive: {round(list_positive,0)} Neutral: {round(list_neutral,0)} Negative: {round(list_negative,0)}"
    )
    sum = list_positive + list_neutral + list_negative
    print(f"All ratings: {sum}")
    print(
        f"{round(list_positive/sum*100,1)}%, {round(list_neutral/sum*100,1)}%, {round(list_negative/sum*100,1)}%"
    )
    # Calculating Output
    var = f"{round(list_positive/sum*100,0)}%, {round(list_neutral/sum*100,0)}%, {round(list_negative/sum*100,0)}%"
    # videosummery=[round(list_positive/sum,3)*100,round(list_neutral/sum,3)*100,round(list_negative/sum,3)*100]
    videohtml = BeautifulSoup(
        requests.get(f"https://youtube.com/watch?v={video_id}").text,
        "html.parser",
    )
    video_title = videohtml.title.get_text().replace(" - YouTube", "")
    videosummery = {
        "titel": video_title,
        "pos": round(list_positive / sum, 2) * 100,
        "neg": round(list_neutral / sum, 2) * 100,
        "neu": round(list_negative / sum, 2) * 100,
        "sum": sum,
    }
    summery.append(videosummery)

import json

with open("data.json", "w") as f:
    json.dump(summery, f)

print(summery)
